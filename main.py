import os,shlex,sys
import pexpect,subprocess

APP_PATH=''
APP_PORT=''
HOST_APP_DIR="./appdata"


def set_permissions(vm_pem):
    cmd = "chmod 400 %s" % vm_pem
    print("cmd  ",cmd)
    return run_command(cmd)

def run_command(cmd):
    cmd=shlex.split(cmd)
    try:
        sp = subprocess.Popen(cmd,stderr=subprocess.PIPE,stdin=subprocess.PIPE,stdout=subprocess.PIPE,
                            universal_newlines=True )
    
    except:
        print("ERROR: Failed to run the command")
        return False
    else:
        _,stderr = sp.communicate()
        if(stderr != '' ):
            print("ERROR: Failed to run the command",stderr)
            return False
        else:
            return True 


def get_app_details_from_vm(vm_user,vm_ip,vm_pem):

    if( not set_permissions(vm_pem)):
        print("ERROR: Unable to set right permissions for key file")
        return
    app_details="~/appdetails"
    
    if(not run_command("mkdir -p %s"%HOST_APP_DIR)):
        print("ERROR: Unable to create directory")
        return False

    cmd="ssh -o StrictHostKeyChecking=no -i %s %s@%s" %(vm_pem,vm_user,vm_ip)

    exp="ubuntu"
    try:
        print("ssh ",cmd)
        child=pexpect.spawn(cmd,timeout=30)
        import pdb
        pdb.set_trace()
        child.expect(exp)
        print("getting process id ",cmd)
        cmd="export PROCESS_ID=`sudo netstat -plant | grep LISTEN.*python3 | awk '{print $7}' | cut -d '/' -f1`"
        child.sendline(cmd)
        child.expect(exp)
        print("getting port")
        cmd="echo `sudo netstat -plant | grep LISTEN.*python3 | awk '{print $4}' | cut -d ':' -f2` > %s" %app_details
        child.sendline(cmd)
        child.expect(exp)
        print("getting app location on vm")
        cmd="echo `sudo lsof | grep python3.*${PROCESS_ID}.*DIR | awk 'NR==1{print $9}'` >> %s" % app_details
        child.sendline(cmd)
        child.expect(exp)
        #child.sendline("echo ${APP_DIRECTORY}")
        #child.expect(expect)
        #cmd = "echo -e ${PORT} \n ${APP_DIRECTORY} > %s" %app_details
        #child.sendline(cmd)
        #child.expect(expect)
        child.close()
    
    except pexpect.TIMEOUT:
        print("ERROR: Timed out ssh")
        return False
    except:
        print("ERROR: Failed to get app details from VM")
        return False
    
    try:
        cmd="scp -o StrictHostKeyChecking=no -i %s %s@%s:%s appdetails.txt" %(vm_pem,vm_user,vm_ip,app_details)
        print("Getting app details from vm to host")
        if( not run_command(cmd) ):
            print("ERROR: Unable to get app details")
            return False

        fp=open("./appdetails.txt","r")

        data=fp.readlines()
        print("Details ",data)
        global APP_PATH,APP_PORT
        APP_PORT=data[0].strip()
        APP_PATH=data[1].strip()

        fp.close()
        
        print("Getting App content")
        cmd="scp -r -o StrictHostKeyChecking=no -i %s %s@%s:\"%s/*\" %s"%(vm_pem,vm_user,vm_ip,APP_PATH,HOST_APP_DIR )

        if( not run_command(cmd)  ):
            print("ERROR: Failed to get app Data")
            return False
        print(APP_PATH,APP_PORT)
        return True
    except:
        print("ERROR: Failed to get data")
        return False
    
    
def build_image(tag):
    import pdb
    pdb.set_trace()

   
    cmd= "docker build -t vmtok8s:%s  --build-arg PORT=%s  -f Dockerfile_template %s"%(tag,APP_PORT,HOST_APP_DIR)
    if(not run_command(cmd)):
        print("ERROR: Failed to build docker image")
        return False
    return True

vm_user="ubuntu"#input("Enter user ")
vm_ip="3.10.117.248" #input("Enter VM's IP: ")
#need to have a function to check if IP is ssh-able
vm_pem="jan6.pem" #input("Enter private key file path ")

if( not os.path.exists(vm_pem) ):
    print("ERROR: Private Keyfile doesn't exist")
    exit()


if( not get_app_details_from_vm(vm_user,vm_ip,vm_pem)  ):
    print("Failed App")
else:
    print("Let's build Docker Image now")
    
    if( not build_image("latest") ):
        print("Image built.....")












